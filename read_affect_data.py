import pyPdf as pp
import csv
from pretty import pprint

file_p = file("bradley1999a.pdf","rb")

pdf_r = pp.PdfFileReader(file_p)

def pop_long_strs(temp):
    res = []
    mult_lines = []
    norm_lines = []
    for each in temp:
        if len(each)> 100:
		   mult_lines.append(each)
        else:
           norm_lines.append(each.replace('(',',').replace(')',','))

    normal_lines = map(split_1st_str,norm_lines)
    return normal_lines,mult_lines

def rem_empty_strs(strng_li):
    tmp = []
    for each in strng_li:
        if each:
            tmp.append(each)
    return tmp

def split_1st_str(str_li):
    li = str_li.split(',')
    if li[0]:
      word,rate = split_alpha_and_num(li[0])
      return [word,rate]+li[1:]

def split_alpha_and_num(st):

    a = []
    for char in st:
        if char.isdigit():
            a.append(st.rindex(char))
    return st[:a[0]],st[a[0]:]

def write_2_csv(data_str):
    if data_str:
     with open('affect_ratings.csv',"wb") as csvfile:
         csv_writ = csv.writer(csvfile)

         for each in data_str:
             if each:
              for row in each:
                if row:
                 write_s = row 
                 csv_writ.writerow(write_s)

def main():

    female_sub_data = []
    male_sub_norm = []
    male_sub_mult = [] 
    raw_data_m = ""
    raw_data_f = ""

    for i in range(4,32):
        raw_data_m += pdf_r.getPage(i).extractText() + "\n"
        temp = raw_data_m.split("\n")
        sub_norm,sub_mult = pop_long_strs(temp)
        male_sub_norm.append(sub_norm)
        male_sub_mult.append(sub_mult)
    male_sub_data = male_sub_norm
    for each in sub_mult:
        b = each.split('Frequency')
        if "Lang" in each:
            temp = each[260:]
            sub_mult.remove(each)
            print temp
        else:
            print each 

#    for i in range(33,46):
#        raw_data_f += pdf_r.getPage(i).extractText() + "\n"
#        temp = raw_data_f.split("\n")
#        female_sub_data += pop_long_strs(temp)
   
    write_2_csv(male_sub_data)
    write_2_csv(female_sub_data)
    

if __name__ == '__main__':
    main()
