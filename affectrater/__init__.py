
from flask import Flask
from params import *
from werkzeug import SharedDataMiddleware
import os

app = Flask(__name__,template_folder=TEMPLATE_PATH,static_folder='static')
app1 = SharedDataMiddleware(app.wsgi_app,{'/':os.path.join(os.path.dirname(__file__),'static')})

app.secret_key = "eoueoue.p,,plglcgl;;kj"

LOGFILE = 'log/affectrater.log'
LOGFILEMODE = 'a'
LOGFILESIZE = 1024 * 1024
LOGFILEBACKUP = 10
LOGFORMAT = "%(asctime)-6s : %(levelname)s - %(message)s"
LOGDATEFMT = "%d/%m/%Y %H:%M:%S"

def check_create_log_file():
    if os.path.exists(os.path.dirname(LOGFILE)):
        return True
    os.makedirs(os.path.dirname(LOGFILE))
    return True                                                                      

if not app.debug:
    import logging,os
    from logging.handlers import RotatingFileHandler
    check_create_log_file()
    logging.basicConfig(level = logging.DEBUG, format = LOGFORMAT,
            datefmt=LOGDATEFMT)
    file_handler = RotatingFileHandler(LOGFILE, LOGFILEMODE, LOGFILESIZE,           LOGFILEBACKUP)                                
    file_handler.setFormatter(logging.Formatter(LOGFORMAT, datefmt=LOGDATEFMT))
    
app.logger.setLevel(logging.INFO)                                       
file_handler.setLevel(logging.INFO)                                     
app.logger.addHandler(file_handler)                                             

import affectrater.views  


