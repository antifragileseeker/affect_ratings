from flask import render_template, session, Response, jsonify, redirect
from flask import request, url_for, flash,send_from_directory
from flask.ext.wtf import Form
from wtforms import TextField
from wtforms.validators import DataRequired
from wtforms.widgets import TextArea

import sent_parser as sp

from affectrater import app
from params import *

class InpForm(Form):
    name = TextField("texttorate",validators=[DataRequired()],widget=TextArea())

@app.route('/',methods=['POST','GET'])
@app.route('/index')
def index_page():
    if request.method == 'GET':
    	inp_form = InpForm(csrf_enabled=True)
    	return render_template('index.tpl',form=inp_form)
    if request.method == 'POST':
    	v_rates =list()
    	a_rates = list()
    	rates = dict()
	
        txt= request.form.get('name')
        rates= sp.word_rate_dict(txt)
        print rates
        for x in rates.values():
            if x:
                 v_rates.append(x[0])
                 a_rates.append(x[1])

        return render_template('plot.tpl',words=rates.keys(),valence_rates =v_rates,arousal_rates=a_rates)

@app.route('/static/<file_path>',methods=['POST','GET'])
def static(**kwargs):
    #print request.path
    print kwargs.get('filename')
    print app.static_folder
    return send_from_directory(app.static_folder,kwargs.get('filename'))


@app.route('/plot',methods=['GET'])
def plot_page():
   
    if request.method == 'GET':
	return render_template('plot.tpl',words=rates.keys(),valence_rates=v_rates,arousal_rates=a_rates)
