from params import *
import numpy as np
#import scipy

import csv,os
import random

rating_data = {} 
valence_means = float()
arousal_means = float()

csvfile = 'male_affect_ratings.csv'

def word_rate_dict(text_inp): 
    import nltk 
    tokens = nltk.word_tokenize(text_inp)
    text = nltk.Text(tokens)
    out_graph = {}
    
    for word in tokens:
        out_graph.update({word:affect_rate(word)})
    return out_graph

def affect_rate(word):
    #print rating_data.get(word)
    if rating_data.get(word):
        return map(lambda x:x-valence_means,rating_data.get(word))

def get_affect_rates():
    global rating_data,valence_means,arousal_means,csvfile
    with open(os.path.join(DATA_FOLDER,csvfile),'rb') as read_fd:
        csvreader = csv.reader(read_fd)
        for row in csvreader:
            
            rating_data.update({row[0]:[float(row[2]),float(row[4])]})

    rang  = range(-100,100)
    return rating_data

def valence_mean():
    valences = list()
    global csvfile
    with open(os.path.join(DATA_FOLDER,csvfile),'rb') as read_fd:
        csvreader = csv.reader(read_fd)
        for row in csvreader:
            valences.append(float(row[3]))
    #return scipy.mean(valences)
    return np.mean(valences)
def arousal_mean():
    arousals= list()
    global csvfile
    with open(os.path.join(DATA_FOLDER,csvfile),'rb') as read_fd:
        csvreader = csv.reader(read_fd)
        for row in csvreader:
            arousals.append(float(row[3]))

    return np.mean(arousals)


import sys
get_affect_rates()

valence_means = valence_mean()
arousal_means = arousal_mean()
#print valence_means,arousal_means
#print word_rate_dict(sys.argv[1])
