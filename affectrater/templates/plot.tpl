<div id="plot_container" style="width:100%; height:400px;"></div>


<script>
$(function (words,valence_rates,arousal_rates) { 
    $('#plot_container').highcharts(
            chart: {
                    type: 'line',
                    marginRight:130,
                    marginBottom:25
                     },
            title: {
                    text: 'Affect Rates'
                    x:-20
                    },
            xAxis: {
                    categories:{{words}} 
                 },
            yAxis: {
                title: {
                        text: 'Arousal and Valency'
                    },
                series: [{
                       data:{{valence_rates}},
                       name: 'valence'
                 },{
                       name: 'arousal',
                       data:{{arousal_rates }}
                }]
           });
     });

</script>
