import nltk

def get_pos(text):
    words = nltk.word_tokenize(text)
    return nltk.pos_tag(words)

def get_ne(pos_tagged_text):
    return nltk.ne_chunk(pos_tagged_text)



